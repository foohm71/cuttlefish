# Cuttlefish

Cuttlefish is an effort to use NLP/ML techniques to find text similarity between issue tickets

## Background

Cuttlefish came out of the [Octopus2](https://gitlab.com/foohm71/octopus2) project. In that project, one of the approaches had the by-product of generating a 100 dimention word embedding generated from training on the ``JIRA_OPEN_DATA_LARGESET.csv`` dataset. An initial trial showed that using this word embedding, we could map Jira ticket (title + description) into a vector in that vector space and thus use that to estimate a cosine distance between that and other Jira tickets similarly mapped. This is described in ``TextSimilarityExplorations.ipynb``.

## Quickstart

This section is for folks who want to get their hands dirty on running the code to see what it's all about. There are 2 ways to do it: (a) using docker (b) running the Jupyter Notebook itself.

### Using Docker

To run the estimator app, do the following:

```docker run --publish 5000:33 --name cuttlefish foohm71/cuttlefish:master```

Make sure:

* You have docker installed
* Port 5000 is not already used

You'll see a whole bunch of stuff on the screen but eventually you'll see something like:
```Status: Downloaded newer image for foohm71/cuttlefish:master
 * Serving Flask app "app" (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
[nltk_data] Downloading package punkt to /root/nltk_data...
[nltk_data]   Unzipping tokenizers/punkt.zip.
 * Running on http://0.0.0.0:33/ (Press CTRL+C to quit)
 ```

 Next you can run the notebook ``testing.ipynb`` and at the end you should see somthing like:
 ```<Response [200]>
[{"id": "12", "title": "mary had a little lamb", "description": "mary had a little lamb its fleece was white as snow", "score": 1.0}, {"id": "15", "title": "the quick brown fox", "description": "hey diddle diddle, the cat and the fiddle", "score": 0.5702062367855973}, {"id": "200", "title": "the cow jumped over the moon", "description": "the rain in spain fell mainly in the plain", "score": 0.650617483439717}]
```

For those who do not want the hassle of installing Jupyter notebooks (ie. Anaconda) just to run a simple test, there is in this repository a ``test.sh`` which does essentially what ``testing.ipynb`` does. 

### Running as Jupyter Notebook

To do this, you will need to run the notebook ``Cuttlefish.ipynb`` like any other notebook. When you reach the cell that has ``app.run()``, you should see something like:

```  * Serving Flask app "__main__" (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
 ```
 and
 ``` 
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
127.0.0.1 - - [07/Dec/2020 15:20:21] "[37mPOST /predict HTTP/1.1[0m" 200 -
```

To test, use the ``testing.ipynb`` like before. 

## A short note of the Jupyter Notebooks here

* ``TextSimilarityExplorations.ipynb`` is meant to run on Google Colab but can be modified to run on a normal Anaconda/Jupyter Notebook
* ``Cuttlefish.ipynb`` is essentially a repackaging into a Flask API but the code are re-purposed from ``TextSimilarityExplorations.ipynb``. 
* In the build process, ``Cuttlefish.ipynb`` is generated into app.py using ``nbconvert`` and ``remove_lines.py``. The details of how that is done can be found in ``.gitlab-ci.yml``. 

## The format of the input payload

The input payload is a JSON that looks like this:

```
{'target': 
   {'id': <id>,
    'title': <title>, 
    'description': <description> 
    }, 
  'corpus':
    [{'id': <id>, 'title': <title>, 'description':<description>},
    :
    {'id': <id>, 'title':<title>, 'description':<description>]
}
```

Where target is the Jira ticket to compare against the "corpus" of existing Jira tickets. 

## The format of the output payload

```
[{"id": <id>, "title": <title>, "description": <description>, "score": 1.0}
:
{"id": <id>, "title": <title>, "description": <description>, "score": <score>}]
```

Where the first item in the array is the original target (hence the score of 1.0) and the rest are the items in the corpus with their similarity scores to the target. 

# Deploying on a Cloud Provider

You may want to deploy the container on a cloud provider and since this is containerized, it makes things a lot easier. 

## AWS

1. Set up ECR (AWS's container registry) and your gitlab CI to deploy there. See https://medium.com/@stijnbe/using-gitlab-ci-with-aws-container-registry-ecaf4a37d791 
2. Follow the instructions to set up a simple AWS Lambda to connect to the container
3. Hook up API Gateway to connect to the lambda

This is a good resource for (2) and (3) - https://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-create-api-as-simple-proxy-for-lambda.html

## GCP

My recommendation here is just to use GKE ie. create a Kubernates cluster and expose it using a GCP service. Follow the instructions in this tutorial to get an idea of how to do that - https://cloud.google.com/kubernetes-engine/docs/tutorials/hello-app 
